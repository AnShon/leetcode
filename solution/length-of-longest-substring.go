package solution

import (
	"fmt"
	"strings"
)

//3. 无重复字符的最长子串
//给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
//
//示例 1:
//
//输入: "abcabcbb"
//输出: 3
//解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
//示例 2:
//
//输入: "bbbbb"
//输出: 1
//解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
//示例 3:
//
//输入: "pwwkew"
//输出: 3
//解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
//请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
func LengthOfLongestSubstringTest() {
	fmt.Println(LengthOfLongestSubstring("aba"))
}
func LengthOfLongestSubstring(s string) int {
	sl := len(s)
	if sl == 0 {
		return 0
	}
	// 寻找子串
	start := 0
	max := 0
	for i := range s {
		v := strings.LastIndex(s[:i], string(s[i]))
		// 重复数据出现
		if v >= start {
			if i-start > max {
				max = i - start
			}
			start = v + 1
		}
	}
	if sl-start > max {
		return sl - start
	}
	return max
}

func LengthOfLongestSubstringV1(s string) int {
	sl := len(s)
	if sl == 0 {
		return 0
	}
	cmap := make(map[string]int)
	// 寻找子串
	start := 0
	max := 0

	for i := range s {
		sc := string(s[i])
		v, ok := cmap[sc]
		// 重复数据出现
		if ok {
			if v >= start {
				if i-start > max {
					max = i - start
				}
				start = v + 1
			}
		}
		cmap[sc] = i
	}
	if sl-start > max {
		return sl - start
	}
	return max
}
func LengthOfLongestSubstringLc(s string) int {
	start, end := 0, 0
	for i := 0; i < len(s); i++ {
		index := strings.Index(s[start:i], string(s[i]))
		if index == -1 {
			if i+1 > end {
				end = i + 1
			}
		} else {
			start += index + 1
			end += index + 1
		}
	}
	return end - start
}
