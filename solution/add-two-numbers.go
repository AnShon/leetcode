package solution

//2. 两数相加
//给出两个 非空 的链表用来表示两个非负的整数。其中，它们各自的位数是按照 逆序 的方式存储的，并且它们的每个节点只能存储 一位 数字。
//
//如果，我们将这两个数相加起来，则会返回一个新的链表来表示它们的和。
//
//您可以假设除了数字 0 之外，这两个数都不会以 0 开头。
//
//示例：
//
//输入：(2 -> 4 -> 3) + (5 -> 6 -> 4)
//输出：7 -> 0 -> 8
//原因：342 + 465 = 807

type ListNode struct {
	Val  int
	Next *ListNode
}

func HeadNode(vals []int) *ListNode {
	headNode := new(ListNode)
	tailNode := headNode
	for _, val := range vals {
		next := new(ListNode)
		next.Val = val
		tailNode.Next = next
		tailNode = next
	}
	return headNode.Next
}
func AddTwoNumbersTest() {
	list1 := HeadNode([]int{1})
	list2 := HeadNode([]int{9, 8})
	list := AddTwoNumbers(list1, list2)
	next := list
	for next != nil {
		println(next.Val)
		next = next.Next
	}
}

func AddTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	return AddTwoNumbersOpt(l1, l2)
}

// 自己的算法
func AddTwoNumbersOpt(l1 *ListNode, l2 *ListNode) *ListNode {
	resultHead := new(ListNode)
	tailNode := resultHead
	carry := 0
	for l1 != nil || l2 != nil {
		v, v1, v2 := 0, 0, 0
		if l1 != nil {
			v1 = l1.Val
		}
		if l2 != nil {
			v2 = l2.Val
		}
		sum := v1 + v2 + carry
		v = sum % 10
		carry = sum / 10
		next := &(ListNode{Val: v})
		tailNode.Next = next
		tailNode = tailNode.Next

		if l1 != nil {
			l1 = l1.Next
		}
		if l2 != nil {
			l2 = l2.Next
		}
	}
	if carry > 0 {
		tailNode.Next = &(ListNode{Val: carry})
	}
	return resultHead.Next
}

func AddTwoNumbersV1(l1 *ListNode, l2 *ListNode) *ListNode {
	resultHead := new(ListNode)
	tailNode := resultHead
	for l1 != nil && l2 != nil {
		next := new(ListNode)
		val := l1.Val + l2.Val
		if val >= 10 {
			tag := val / 10
			val = val % 10
			if l1.Next != nil {
				l1.Next.Val = l1.Next.Val + tag
			} else if l2.Next != nil {
				l2.Next.Val = l2.Next.Val + tag
			} else {
				l1.Next = new(ListNode)
				l1.Next.Val = tag
			}
		}
		next.Val = val
		tailNode.Next = next
		tailNode = next
		l1 = l1.Next
		l2 = l2.Next
	}
	if l1 != nil {
		tailNode.Next = l1
		for l1.Val >= 10 {
			tag := l1.Val / 10
			l1.Val = l1.Val % 10
			if l1.Next != nil {
				l1.Next.Val = l1.Next.Val + tag
			} else {
				next := new(ListNode)
				next.Val = tag
				l1.Next = next
			}
			l1 = l1.Next
		}
	} else if l2 != nil {
		tailNode.Next = l2
		for l2.Val >= 10 {
			tag := l2.Val / 10
			l2.Val = l2.Val % 10
			if l2.Next != nil {
				l2.Next.Val = l2.Next.Val + tag
			} else {
				next := new(ListNode)
				next.Val = tag
				l2.Next = next
			}
			l2 = l2.Next
		}
	}
	return resultHead.Next
}

// leetcode 上的算法
func AddTwoNumbersLC(l1 *ListNode, l2 *ListNode) *ListNode {
	resultHead := new(ListNode)
	tailNode := resultHead
	sum := 0
	for l1 != nil || l2 != nil || sum > 0 {
		next := new(ListNode)
		tailNode.Next = next
		tailNode = next

		if l1 != nil {
			sum = l1.Val + sum
			l1 = l1.Next
		}
		if l2 != nil {
			sum = l2.Val + sum
			l2 = l2.Next
		}
		next.Val = sum % 10
		sum = sum / 10
	}
	return resultHead.Next
}
