module leetcode

go 1.14

require leetcode/solution v0.0.1-local

replace leetcode/solution => ./solution
